## PROJECT INFO

TASK MANAGER

## DEVELOPER INFO

**Name:** Svetlana Mironova

**E-mail:** smironova@tsconsulting.com

**E-mail:** ssmironova@vtb.ru

## HARDWARE

**CPU:** i5

**RAM:** 8G

## SOFTWARE

**Version JDK:** 15.0.1

## PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

## SCREENSHITS

https://drive.google.com/drive/folders/1VRgKMfSin0OWTMAjzoYNTYRGOvkgd_p5?usp=sharing
