package com.tsc.smironova.tm.constant;

public class TerminalConstant {

    public static final String ABOUT = "about";
    public static final String VERSION = "version";
    public static final String HELP = "help";

}
